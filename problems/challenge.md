#### Twoje zadanie

Stwórz plik o nazwie `{{name}}.js`.

Wykonaj zadanie wg powyższego polecenia, kod umieść w pliku zgodnie z wytycznymi (np. poprawna nazwa funkcji).

Sprawdź napisane rozwiązanie za pomocą następującej komendy:
```
npx pwjs-exercises verify {{name}}.js
```
