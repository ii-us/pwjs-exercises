let _correct = 0;
let _failed = 0;
let _failedTests = [];
const test = (id, test) => { if (test) { _correct++; } else { _failed++; _failedTests.push(id); }; };
let spy1 = Spy(console, 'log');
let spy2 = Spy(console, 'error');
console.log('calling console.log')
console.log('calling console.log')
console.error('calling console.error')
test(1, spy1.count === 2);
test(2, spy2.count === 1);
[_correct, _failed, _failedTests];
