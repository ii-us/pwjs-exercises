let _correct = 0;
let _failed = 0;
let _failedTests = [];
const test = (id, test) => { if (test) { _correct++; } else { _failed++; _failedTests.push(id); }; };
test(1, JSON.stringify(countWords(['Apple', 'Banana', 'Apple', 'Durian', 'Durian', 'Durian'])) === JSON.stringify({ Apple: 2, Banana: 1, Durian: 3 }));
test(2, JSON.stringify(countWords(['Apple', 'Banana'])) === JSON.stringify({ Apple: 1, Banana: 1 }));
test(3, JSON.stringify(countWords([])) === JSON.stringify({}));
[_correct, _failed, _failedTests];