let _correct = 0;
let _failed = 0;
let _failedTests = [];
const test = (id, test) => { if (test) { _correct++; } else { _failed++; _failedTests.push(id); }; };
test(1, reduce([1,2,3], (x,y) => x+y, 0) === 6);
test(2, reduce([], (x,y) => x+y, 0) === 0);
test(3, reduce(['a','b','c'], (x,y) => x+y, '') === 'abc');
[_correct, _failed, _failedTests];