let _correct = 0;
let _failed = 0;
let _failedTests = [];
const test = (id, test) => { if (test) { _correct++; } else { _failed++; _failedTests.push(id); }; };
test(1, JSON.stringify(getShortMessages([
    {message: 'Ut vel in.'},
    {message: 'Maecenas interdum risus a sagittis lacinia integer.'}
])) === JSON.stringify([
    'Ut vel in.'
]));
test(2, JSON.stringify(getShortMessages([
])) === JSON.stringify([
]));
test(3, JSON.stringify(getShortMessages([
    {message: 'Sed at eros ut mi dapibus cursus et vitae ex biam.'},
    {message: 'Curabitur convallis ac lectus sed fringilla ligula.'}
])) === JSON.stringify([
    'Sed at eros ut mi dapibus cursus et vitae ex biam.'
]));
[_correct, _failed, _failedTests];