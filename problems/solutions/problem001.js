
function triangleArea(a, b, c) {
    if(a > 0 && b > 0 && c > 0 && a + b > c && b + c > a && a + c > b) {
        const p = (a+b+c)/2;
        return Math.round(Math.sqrt(p*(p-a)*(p-b)*(p-c))*100)/100;
    }
    return -1;  
}