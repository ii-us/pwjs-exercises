function strangeSum(a, b) {
    var c = Math.round(a);
    var d = Math.round(b);
    if(c != a || d != b) {
        return null;
    } else if(a === b) {
        return (a+b)*3;
    }
    return a+b;
}
