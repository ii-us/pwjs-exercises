function countWords(inputWords) {
	return inputWords.reduce((prev, curr) => {
		if (curr in prev === false) prev[curr] = 0;
		prev[curr]++;
		return prev;
	}, {});
}
