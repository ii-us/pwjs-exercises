function getShortMessages(messages) {
	return messages.map(_ => _.message).filter(_ => _.length <= 50);
}
