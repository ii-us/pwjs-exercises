function stringRotate(a, b) {
    if (typeof b != 'number' || b < 0) {
        return "";
    }
    return a.slice(b, a.length) + a.slice(0, b);
}
