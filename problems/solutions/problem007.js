function findMax(a) {
    let max = Number.NEGATIVE_INFINITY;
    for(let i = 0; i < arguments.length; i++) {
        if(arguments[i] > max) {
            max = arguments[i];
        }
    }
    return max;
}
