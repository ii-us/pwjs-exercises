function objectsDiff(a, b) {
  let result = [];
  for (prop in a) {
    if (!b.hasOwnProperty(prop)) {
      result.push(prop);
    }
  }
  for (prop in b) {
    if(! a.hasOwnProperty(prop)) {
      result.push(prop);
    }
  }
  return result;
}
