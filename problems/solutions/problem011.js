function deepFindAndCount(a, b) {
    let result = 0;
    if (Array.isArray(a)) {
        for (let i = 0; i < a.length; i++) {
            result += deepFindAndCount(a[i], b);
        }
    } else if(a === b) {
        result += 1;
    }
    return result
}
