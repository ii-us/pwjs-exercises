function parametricSort(a, b) {
	if (b === "asc") {
		a.sort();
		return a;
	} else if (b === "desc") {
		a.sort().reverse();
		return a;
	}
	return false;
}
