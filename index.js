const fs = require('fs');
const path = require('path');
const { VM } = require('vm2');

const jsing = require('workshopper-adventure')({
    appDir: __dirname,
    languages: ['pl'],
    header: require('workshopper-adventure/default/header'),
    footer: require('workshopper-adventure/default/footer')
});

const problems = fs.readFileSync(path.join(__dirname, 'problems', 'problems.md'), 'utf8').split('### ').filter(_ => _).map(problemDescription => {
    const lines = problemDescription.split('\n');
    const name = lines[0].trim();
    return {
        name,
        description: lines.slice(1).join("\n").trim()
    }
});

jsing.addAll(problems.map((problem) => {
    return {
        name: problem.name,
        fn: () => {
            const solutionFilepath = path.join(__dirname, 'problems', 'solution') + '.md';
            const jsSolutionFilepath  = path.join(__dirname, 'problems', 'solution') + '.js';
            const troubleshootingFilepath   = path.join(__dirname, 'problems', 'troubleshooting') + '.md';
            const challengeDescription = fs.readFileSync(path.join(__dirname, 'problems', 'challenge.md'), 'utf8')
                .replace(/\{\{name\}\}/g, problem.name);

            const exports = {}
            exports.init = function (workshopper) {
                this.problem = problem.description + "\n" + challengeDescription;
                this.solution = { file: solutionFilepath };
                this.solutionPath = jsSolutionFilepath;
                this.troubleshootingPath = troubleshootingFilepath;
            };
            exports.verify = function (args, cb) {
                const studentScriptPath = path.resolve(process.cwd(), args[0]);
                const testScriptPath = path.join(__dirname, 'problems', 'tests', problem.name + '.js');
                if (fs.existsSync(studentScriptPath) === false) {
                    exports.fail = [{ text: 'The student\'s script does not exist.', type: 'md' }];
                    cb(null, false);
                    return;
                }
                if (fs.existsSync(testScriptPath) === false) {
                    exports.fail = [{ text: 'The test\'s script does not exist.', type: 'md' }];
                    cb(null, false);
                    return;
                }
                const studentScript = fs.readFileSync(studentScriptPath, 'utf8');
                const testScript = fs.readFileSync(testScriptPath, 'utf8');
                try {
                    let vm = new VM({
                        timeout: 1000,
                        sandbox: {},
                        console: 'off'
                    });
                    let testResult = vm.run(studentScript + "\n" + testScript);
                    if (testResult[1] > 0) {
                        exports.fail = [
                            { text: 'The tests failed.', type: 'md' },
                            {
                                text: 'Correct: ' + testResult[0] + "\n\n"
                                    + 'Failed: ' + testResult[1] + "\n\n"
                                    + 'Failed test: ' + testResult[2].toString(),
                                type: 'md'
                            }
                        ];
                        cb(null, false);
                        return;
                    } else {
                        return cb(null, true);
                    }
                } catch (exception) {
                    exports.fail = [{ text: exception.toString(), type: 'md' }];
                    cb(null, false);
                }
                return;
            };
            return exports;
        }
    }
}));

module.exports = jsing;