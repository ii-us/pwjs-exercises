const fs = require('fs');
const path = require('path');
const { VM } = require('vm2');

const problems = fs.readFileSync(path.join(__dirname, 'problems', 'problems.md'), 'utf8').split('### ').filter(_ => _).map(problemDescription => {
    const lines = problemDescription.split('\n');
    const name = lines[0].trim();
    return name;
});

const studentScriptDir = process.argv[2] || '';

let result = [];

for (let problem of problems) {
    let studentScriptPath = path.join(studentScriptDir, problem + '.js');
    let testScriptPath = path.join(__dirname, 'problems', 'tests', problem + '.js');
    if (fs.existsSync(studentScriptPath) === false) {
        result.push([problem, 'failed', [0, 0, []], 'The student\'s script does not exist.', '']);
        continue;
    }
    if (fs.existsSync(studentScriptPath) === false) {
        result.push([problem, 'failed', [0, 0, []], 'The test\'s script does not exist.', '']);
        continue;
    }
    let studentScript = fs.readFileSync(studentScriptPath, 'utf8');
    let testScript = fs.readFileSync(testScriptPath, 'utf8');
    try {
        let vm = new VM({
            timeout: 1000,
            sandbox: {},
            console: 'off'
        });
        let testResult = vm.run(studentScript + "\n" + testScript);
        if (testResult[1] > 0) {
            result.push([problem, 'failed', testResult, 'The tests failed.']);
        } else {
            result.push([problem, 'success', testResult, 'OK']);
        }
    } catch (exception) {
        result.push([problem, 'failed', [0, 0, []], exception.toString()]);
    }
}

for (let record of result) {
    console.log("Problem: " + record[0] + ", status: " + record[1] + ", tests: [" + record[2][0] + ", "
        + record[2][1] + ", " + record[2][2].join('; ') + "], description: " + record[3]);
}
