# PwJS Exercises

## Instalacja Node.js

Upewnij się, że masz zaintalowany Node.js na swojej maszynie.

W razie potrzeby zainstaluj go z strony [nodejs.org](https://nodejs.org/).

## Uruchomienie wirtualnego laboratorium

Otwórz terminal i wpisz następujące komendy:

```
mkdir pwjs-exercises
cd pwjs-exercises
npm i pwjs-exercises
npx pwjs-exercises
```

Nawiguj w menu za pomocą strzałek, po wybraniu zadania naciśnij enter.

## Rozwiązywanie zadań

Postępuj zgodnie z opisem zadań. Pamiętaj o odpowiednich nazwach plików odpowiadających nazwom problemów np. `problem001`, `problem002`, itd. W większości zadań, wystaczy, że plik z rozwiązaniem zawiera funkcję opisaną w zadaniu. Skrypty muszą być kodowane UTF8.
